import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node {
  /***
   * Koostage meetodid puu parempoolse suluesituse (String) järgi puu moodustamiseks (parsePostfix, tulemuseks puu juurtipp)
   * ning etteantud puu vasakpoolse suluesituse leidmiseks stringina
   * (puu juureks on tipp  this, meetod leftParentheticRepresentation tulemust ei trüki, ainult tagastab String-tüüpi väärtuse).
   * Testige muuhulgas ka ühetipuline puu. Testpuude moodustamine ja tulemuse väljatrükk olgu peameetodis.
   * Puu kujutamisviisina kasutage viidastruktuuri (viidad "esimene alluv"  firstChild ja "parem naaber"  nextSibling, tipu tüüp on  Node).
   * Tipu nimi ei tohi olla tühi ega sisaldada ümarsulge, komasid ega tühikuid.
   * Kui meetodile  parsePostfix etteantav sõne on vigane, siis tuleb tekitada asjakohase veateatega  RuntimeException.
   */
   /*
   (((2,1)-,4)*,(69,3)/)+
   +(*(-(2,1),4),/(69,3))
   ||
   */
  private String name;
  private Node firstChild;
  private Node nextSibling;

  Node(String n, Node d, Node r) {
    name = n;
    firstChild = d;
    nextSibling = r;
  }

  public static Node parseTree(Node node, String s) {
    System.out.println("Input string: " + s);
    System.out.println("------------------------------> START");
    String rootName = "";
    if (hasParentheses(s)) {
      rootName = charAfterParentheses(s);
      System.out.println("RootName: " + rootName);
    } else {
      rootName = s;
      System.out.println("RootName: " + rootName);
    }

    //node = new Node(rootName, parseTree(node, firstNestingString(s)), null);
    node = new Node(rootName, null, null);
    //Node root = parseTree(root, s);
    if (hasParentheses(s)) {


      if (hasParentheses(firstNestingString(s))) {
        System.out.println("/// Parentheses: " + s);
        if (hasComma(firstNestingString(s))) {
          System.out.println("/// Parentheses and comma: " + s);
          for (int i = firstNestingString(s).length() - 1; i >= 0; i--) {
            System.out.println("firstNestingString(s): " + firstNestingString(s));
            // TODO -> Tee dynaamiline siblingute loomine, Hetkel ainult vasak ja parem, aga neid v6ib olla rohkem, kui kaks

            if (firstNestingString(s).charAt(i) == ',' && nestingDepthFromLeft(firstNestingString(s).substring(0, i))[1] == 0) {
              System.out.println("Leidsin koma kohal i: " + firstNestingString(s).charAt(i) + " " + i);
              String leftSibling = firstNestingString(s).substring(0, i);   System.out.println("leftSibling: " + firstNestingString(s).substring(0, i));
              String rightSibling = firstNestingString(s).substring(i + 1); System.out.println("rightSibling: " + firstNestingString(s).substring(i + 1));

              if (hasParentheses(leftSibling) && !(hasParentheses(rightSibling))) {
                Node right = new Node(rightSibling, null, null);
                Node left = parseTree(node, leftSibling);
                node.firstChild = left;
                left.nextSibling = right;

              } else if (!hasParentheses(leftSibling) && (hasParentheses(rightSibling))) {
                Node right = parseTree(node, rightSibling);
                Node left = new Node(leftSibling, null, right);
                node.firstChild = left;

              } else {                                                      System.out.println("m6lemal siblingul on sulud");
                Node right = parseTree(node, rightSibling);
                Node left = parseTree(node, leftSibling);

                //Node right = new Node(rightSibling, null, null);
                //Node left = new Node(leftSibling, null, right);
                node.firstChild = left;
                left.nextSibling = right;                                   System.out.println("*-*-*-*");
              }
            } else {}
          }
          //find siblings, separate by comma
          System.out.println("**** nestingR: " + nestingDepthFromRight(firstNestingString(s))[1]);

        } else {                                                            System.out.println("/// Parentheses and no comma: " + s);
          if (firstNestingString(s).indexOf(')') > firstNestingString(s).indexOf('(')) {
            node.firstChild = parseTree(node, firstNestingString(s));       System.out.println("-*-*- node.firstChild.name: " + node.firstChild.name);
          }
        }
      } else {                                                              // sulge enam pole, otsi edasi, 'kki on ilma nextsiblinguteta noded
        if (hasComma(firstNestingString(s))) {                              System.out.println("/// No parentheses + comma: " + firstNestingString(s));
          String[] siblings = firstNestingString(s).split(",");      System.out.println("//// SIIN ALGAB SIBLINGUTE DYNAAMILINE LOOMINE");

          // TODO dynamic children generation
          if (siblings.length > 2) {
            int countSiblings = siblings.length;                            System.out.println("Siblinguid on rohkem, kui 2: " + "countSiblings : " + countSiblings);
            String leftSibling = siblings[0];
            node.firstChild = new Node(leftSibling, null, null);
            //node.firstChild = parseTree(node, leftSibling);

            for (int i = 1; i < countSiblings; i++) {
              String nextSibling = siblings[i]; /* C,D,E,F */               System.out.println("!!! node.firstChild: " + node.firstChild.name);
              node.firstChild.nextSibling = parseTree(node, nextSibling);   System.out.println("!!! node.firstChild.nextSibling: " + node.firstChild.nextSibling.name);
              Node next = new Node(node.firstChild.name, null, node.firstChild.nextSibling);
              node.firstChild.name = nextSibling;                           System.out.println("*-*-*-*");
            }
            return node;
          } else {                                                          System.out.println("Siblinguid on 2");
            String leftSibling = siblings[0];                               System.out.println("left sib: " + leftSibling);
            String rightSibling = siblings[1];                              System.out.println("right sib: " + rightSibling);
            Node right = new Node(rightSibling, null, null);
            Node left = new Node(leftSibling, null, right);
            node.firstChild = new Node(leftSibling, null, right);
            return node;
          }
        } else {                                                            System.out.println("/// No parentheses + No comma: " + s);
          node.name = rootName;
          node.firstChild = new Node(firstNestingString(s), null, null);
          return node;
        }
      }
    } else {
      node.name = rootName;
      return node;
    }
    System.out.println("L6pp!!!!!");
    return node;
  }


  public static Node parsePostfix(String s) {
    if (isValidInputString(s)) {
      System.out.println("!!! VALID INPUT !!!");
      Node root = parseTree(null, s);
      System.out.println("parseTree(parentNode, input string");
      return root;
    } else {
      throw new RuntimeException("Input string is not valid: " + s);
    }
  }

  public static boolean isValidInputString(String s) {
    String trimmedS = s.trim().replaceAll("\\s+", "");
    if (s != null && s.length() > 0 && s == trimmedS) {
      if (hasParentheses(s)) {
        System.out.println("Inputil on sulud");
        if (hasCorrectPairParentheses(s)) {
          System.out.println("Korrektsed sulud");
          if (hasLeafsWithoutParents(s)) {
            System.out.println("viga sulgudega");
            throw new RuntimeException("Input has leafs without parent nodes: " + s);
          } else {
            System.out.println("viga sulgudega pole");
            if (!hasCorrectRootOnRightSide(s)) {
              return false;
            } else if (hasComma(firstNestingString(s))) {  // SULGUDES ON KOMA
              if (!hasCommaAtEndOrBeginning(firstNestingString(s)) &&
                  hasSingleCommasInRow(firstNestingString(s))) {
                System.out.println("Inputil pole komasid sulgude sees alguses v] l]pus ja komasid pole jrjest yle yhe");
                return true;
              } else {
                System.out.println("Inputil on komad vales kohas, v]i on neid liiga palju");
                return false;
              }
            } else { /// SULGUDES POLE KOMASID
              System.out.println("Inputil on sulud, aga komasid pole: " + firstNestingString(s));
              if (firstNestingString(s).length() == 0) {
                System.out.println("Sulud seest tyhjad");
                return false;
              } else {
                System.out.println("Sulgudes sisu pikkus: " + firstNestingString(s).length());
              }
            }
          }
        } else {
          return false;
        }
      } else {
        System.out.println("Inputil pole sulge");
        if (hasComma(s)) {
          System.out.println("Input is incorrect: " + s + " has commas in root and no parentheses");
          return false;
        }
        System.out.println("Kui pole ka komasid, siis on single root");
      }
      return true;
    } else {
      return false;
    }
  }

  public static boolean hasLeafsWithoutParents(String s) {
    for (int i = 0; i <= s.length() - 1; i++) {
      if (s.charAt(i) == ')' && s.charAt(i + 1) == '(') {
        System.out.println("hasLeafsWithoutParents");
        return true;
      } else if (s.charAt(i) == ')' && s.charAt(i + 1) == ',' && s.charAt(i + 2) == '(') {
        System.out.println("hasLeafsWithoutParents comma");
        return true;
      }
    }
    return false;
  }

  public static boolean hasParentheses(String s) {
    Pattern p = Pattern.compile("([\\)|\\(])");
    Matcher m = p.matcher(s);
    return m.find();
  }

  public static boolean hasCorrectPairParentheses(String s) {
    int countRightParentheses = 0;
    int countLeftParentheses = 0;
    if (hasParentheses(s)) {
      // TODO => kontrolli, et poleks topelt sulge
      if (hasEmptyNestingLevels(s)) {
        return false;
      } else {
        System.out.println("no empty levels");
        System.out.println("sulud: " + s);
        for (int i = 0; i <= s.length() - 1; i++) {
          int currentNestingDepth = nestingDepthFromLeft(s.substring(0, i + 1))[1];
          if (s.charAt(0) == '(') {
            // TODO -> current nesting on vale?
            if (s.charAt(i) == '(') {
              countLeftParentheses++;
              if (i != 0 && s.charAt(i) == '(' && currentNestingDepth == 0) {
                System.out.println("String has multiple roots: " + s);
                return false;
              }
            }
            if (s.charAt(i) == ')') {
              countRightParentheses++;
            }
          } else {
            throw new RuntimeException("Starts with wrong parentheses: " + s);
          }
        }
      }
      return countRightParentheses == countLeftParentheses; // tagastab, kas sulgude arv on 6ige
    } else {
      return false;
    }
  }

  public static boolean hasEmptyNestingLevels(String s) {
    Pattern p1 = Pattern.compile("([\\(\\)])\\1{1,}");
    Pattern p2 = Pattern.compile("([\\)])\\1{1,}");
    Matcher m1 = p1.matcher(s);
    Matcher m2 = p2.matcher(s);

    if (m1.find() && m2.find()) {
      return true;
    }
    return false;
  }

  public static int indexLeftParentheses(String s) {
    int indexLeft = 0;
    for (int j = 0; j < s.length(); j++) {
      if (s.charAt(j) == '(') {
        indexLeft = j;
        break;
      }
    }
    return indexLeft;
  }

  public static int indexRightParentheses(String s) {
    int indexRight = 0;
    for (int k = s.length() - 1; k >= 0; k--) {
      if (s.charAt(k) == ')') {
        indexRight = k;
        break;
      }
    }
    return indexRight;
  }

  public static boolean hasComma(String s) {
    Pattern pattern = Pattern.compile("[\\,]");
    Matcher matcher = pattern.matcher(s);
    return matcher.find();
  }

  private static boolean hasCommaAtEndOrBeginning(String s) {
    return (s.indexOf(',') == 0 || s.indexOf(',') == s.length() - 1);
  }

  private static boolean hasSingleCommasInRow(String s) {
    Pattern p = Pattern.compile("((?:[,])+[,]|[,]{2})");
    Matcher m = p.matcher(s);
    if (m.find()) {
      return false;
    } else {
      return true;
    }
  }

  public static String charAfterParentheses(String s) {
    if (hasParentheses(s)) {
      String rootName = s.substring(indexRightParentheses(s) + 1);
      return rootName;
    }
    return "";
    //throw new RuntimeException("String out of bounds");
  }

  private static boolean hasCorrectRootOnRightSide(String s) {
    String rootString = s.substring(indexRightParentheses(s) + 1);
    System.out.println("rootString: " + rootString);
    if (hasComma(rootString)) {
      return false;
    }
    return true;
  }

  private static String firstNestingString(String s) {
    if (hasParentheses(s)) {
      if (s.length() > 2 && indexLeftParentheses(s) != -1 && indexRightParentheses(s) != -1) {
        String firstNestingString = s.substring(indexLeftParentheses(s) + 1, indexRightParentheses(s));
        //System.out.println("firstNestingString: " + firstNestingString);
        return firstNestingString;
      }
      System.out.println("ELSE///");
    }
    return s;
  }

  public static int[] nestingDepthFromRight(String s) {
    int countTotalNesting = 0;
    int currentNesting = 0;

    for (int i = s.length() - 1; i > 0; i--) {
      if (s.charAt(i) == ')') {
        currentNesting++;
        if (currentNesting > countTotalNesting) {
          countTotalNesting++;
        }
      }
      if (s.charAt(i) == '(') {
        currentNesting--;
      }
      if (i != 0 && s.charAt(i) == '(' && currentNesting == 0 && s.substring(i, s.length() - 1).contains(")")) {
        //throw new RuntimeException("There is nesting, but multiple ROOTS: " + s);
      }
    }
    int[] nestingData = {countTotalNesting, currentNesting};
    return nestingData;
  }

  public static int[] nestingDepthFromLeft(String s) {
    int countTotalNesting = 0;
    int currentNesting = 0;

    for (int i = 0; i < s.length() - 1; i++) {
      if (s.charAt(i) == '(') {
        currentNesting++;
        if (currentNesting > countTotalNesting) {
          countTotalNesting++;
        }
      }
      if (s.charAt(i) == ')') {
        currentNesting--;
      }
      if (i != 0 && s.charAt(i) == ')' && currentNesting == 0 && s.substring(i, s.length() - 1).contains("(")) {
        //throw new RuntimeException("There is nesting, but multiple ROOTS: " + s);
      }
    }
    int[] nestingData = {countTotalNesting, currentNesting};
    return nestingData;
  }

  public String getName() {
    return name;
  }

  public String leftParentheticRepresentation() {
    StringBuffer b = new StringBuffer();
    b.append(getName());

    if (firstChild == null) {
      System.out.println("firstchild null, root on tyhi");
      return name;
    }

    b.append('(');

    if (firstChild != null) {
      System.out.println("firstchild not null");
      b.append(firstChild.leftParentheticRepresentation());

      if (firstChild.nextSibling != null) {
        System.out.println("firstChild.nextSibling != null");
        b.append(",");
      }
    }

    Node next = firstChild.nextSibling;
    while (next != null) {
      System.out.println("while next != null");
      b.append(next.leftParentheticRepresentation());
      next = next.nextSibling;

      if (next != null) {
        b.append(",");
        System.out.println("next != null");
      }
    }
    b.append(')');

    System.out.println(b.toString());
    return b.toString(); //  TODO!!! return the string without spaces
  }

  public void setNextSibling(Node p) {
    nextSibling = p;
  }

  public Node getNextSibling() {
    return nextSibling;
  }

  public void setFirstChild(Node a) {
    firstChild = a;
  }

  public Node getFirstChild() {
    return firstChild;
  }

  //@Override
  /*public String toString() {
    return "TODO!!!";
  }*/

  public void processTreeNode() {
    System.out.print(getName() + "  ");
  }

  public boolean hasNext() {
    return (getNextSibling() != null);
  }

  public Node next() {
    return getNextSibling();
  }

  public Iterator<Node> children() {
    return (Iterator<Node>) getFirstChild();
  }

  public void addChild(Node a) {
    if (a == null) return;
    Iterator<Node> children = children();
    if (children == null)
      setFirstChild(a);
    else {
      while (children.hasNext())
        children = (Iterator<Node>) children.next();
      ((Node) children).setNextSibling(a);
    }
  }


  public static void main(String[] param) {
      /*String s = "(B1,C,D)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)*/
    //String s = "(((2,1)-,4)*,(6,3)/)+";
    String s = "(B,C,D,E,F)A"; // => A(B,C,D,E,F)
    //String s = "((A)(B)C)D";
    //String s = "((A),(B)C)D";
    //String s = "((A)E,(B)C)D";
    Node t = Node.parsePostfix(s);
    String v = t.leftParentheticRepresentation();
    System.out.println(s + " ==> " + v);

  }
}

